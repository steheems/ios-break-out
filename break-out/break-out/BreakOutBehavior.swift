//
//  BreakOutBallBehavior.swift
//  break-out
//
//  Created by Erwin Heemsbergen on 18-05-15.
//  Copyright (c) 2015 Gerrit Ruiter. All rights reserved.
//

import UIKit
import CoreMotion

protocol BreakOutBehaviorDelegate {
    /**
     * Function gets called everytime a brick is destroyed
     */
    func brickDestroyed(BrickView)
    
    /**
     * Function gets called when the ball goes bellow the paddle
     */
    func loseLife()
}

class BreakOutBehavior : UIDynamicBehavior, UICollisionBehaviorDelegate {
    let manager = CMMotionManager()
    
    let gravity = UIGravityBehavior()
    
    let push = UIPushBehavior()
    
    var paddle: UIView?
    
    var ball: BallView?
    
    var delegate: BreakOutBehaviorDelegate?
    
    lazy var collider: UICollisionBehavior = {
        let lazilyCreatedCollisionBehavior = UICollisionBehavior()
        lazilyCreatedCollisionBehavior.translatesReferenceBoundsIntoBoundary = true
        lazilyCreatedCollisionBehavior.collisionDelegate = self
        return lazilyCreatedCollisionBehavior
        }()
    
    lazy var brickBehavior: UIDynamicItemBehavior = {
        let lazilyCreatedBrickBehavior = UIDynamicItemBehavior()
        lazilyCreatedBrickBehavior.allowsRotation = false
        lazilyCreatedBrickBehavior.elasticity = 0
        lazilyCreatedBrickBehavior.resistance = 1
        lazilyCreatedBrickBehavior.friction = 1
        return lazilyCreatedBrickBehavior
        }()
    
    lazy var ballBehavior: UIDynamicItemBehavior = {
        let lazilyCreatedBallBehavior = UIDynamicItemBehavior()
        lazilyCreatedBallBehavior.allowsRotation = true
        lazilyCreatedBallBehavior.angularResistance = 0.5
        lazilyCreatedBallBehavior.elasticity = CGFloat(SettingsManager().ballBounciness)
        lazilyCreatedBallBehavior.resistance = 0
        lazilyCreatedBallBehavior.friction = 0
        return lazilyCreatedBallBehavior
        }()
    
    var bounciness: Float? {
        didSet {
            if bounciness != nil {
                ballBehavior.elasticity = CGFloat(bounciness!)
            }
        }
    }
    var gravityStrength: Float? {
        didSet {
            push.setAngle(CGFloat(90 / (180 / M_PI)), magnitude: CGFloat(gravityStrength!))
        }
    }
    
    var useGravity: Bool? {
        didSet {
            if useGravity == true {
                addChildBehavior(gravity)
                
                if manager.deviceMotionAvailable {
                    manager.deviceMotionUpdateInterval = 0.75
                    manager.startDeviceMotionUpdatesToQueue(NSOperationQueue.mainQueue()) {
                        [weak self] (data: CMDeviceMotion!, error: NSError!) in
                        let rotation = CGFloat(atan2(data.gravity.x, data.gravity.y) - M_PI/2)
                        let zAxis = -1 / data.gravity.z - 1
                        
                        if zAxis >= 0 && zAxis <= 1 {
                            self!.gravity.magnitude = CGFloat(zAxis * Double(self!.gravityStrength!))
                        }
                        
                        self!.gravity.angle = rotation
                    }
                }
            } else {
                removeChildBehavior(gravity)
                manager.stopDeviceMotionUpdates()
            }
        }
    }
    
    override init() {
        super.init()
        useGravity = SettingsManager().gravityUsesSensor
        bounciness = SettingsManager().ballBounciness
        gravityStrength = SettingsManager().gravityStrength
        
        addChildBehavior(push)
        addChildBehavior(collider)
        addChildBehavior(ballBehavior)
        addChildBehavior(brickBehavior)
        
//        gravity.magnitude = CGFloat(0.5)
//        gravity.angle = CGFloat(90 / (180 / M_PI))
        
        push.setAngle(CGFloat(90 / (180 / M_PI)), magnitude: CGFloat(gravityStrength!))
        
        if SettingsManager().gravityUsesSensor {
            addChildBehavior(gravity)
            
            if manager.deviceMotionAvailable {
                manager.deviceMotionUpdateInterval = 0.75
                manager.startDeviceMotionUpdatesToQueue(NSOperationQueue.mainQueue()) {
                    [weak self] (data: CMDeviceMotion!, error: NSError!) in
                    let rotation = CGFloat(atan2(data.gravity.x, data.gravity.y) - M_PI/2)
                    let zAxis = -1 / data.gravity.z - 1
                    
                    if zAxis >= 0 && zAxis <= 1 {
                        self!.gravity.magnitude = CGFloat(zAxis)
                    }
                    
                    self!.gravity.angle = rotation
                }
            }
        }
    }
    
    func addPush(x: CGFloat, y: CGFloat) {
        push.pushDirection = CGVector(dx: x, dy: y)
        push.magnitude = -1
        
        NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: Selector("removePush"), userInfo: nil, repeats: false)
    }
    
    func removePush() {
        push.setAngle(CGFloat(90 / (180 / M_PI)), magnitude: CGFloat(gravityStrength!))
    }
    
    func collisionBehavior(behavior: UICollisionBehavior, beganContactForItem item1: UIDynamicItem, withItem item2: UIDynamicItem, atPoint p: CGPoint) {
        //push.removeItem(item2)
        if var ball = item1 as? BallView {
            if var brick = item2 as? BrickView {
                removeBrick(item2 as! BrickView)
            } else if var bottomLoseBar = item2 as? BottomLoseBarView {
                removeBall()
                delegate?.loseLife()
            }
        
        } else if var ball = item2 as? BallView {
            if var brick = item1 as? BrickView {
                removeBrick(item1 as! BrickView)
            } else if var bottomLoseBar = item1 as? BottomLoseBarView {
                removeBall()
                delegate?.loseLife()
            }
        }
    }
    
    func collisionBehavior(behavior: UICollisionBehavior, beganContactForItem item: UIDynamicItem, withBoundaryIdentifier identifier: NSCopying, atPoint p: CGPoint) {
        let identOpt : NSCopying? = identifier
        if let ident = identOpt {
            if ident as! String == "BottomLoseBar" {
                removeBall()
                delegate?.loseLife()
            }
        }
        //push.removeItem(item)
    }
    
    func addBarrier(path: UIBezierPath, name: String) {
        collider.removeBoundaryWithIdentifier(name)
        collider.addBoundaryWithIdentifier(name, forPath: path)
    }
    
    func addPaddle(paddle: UIView) {
        self.paddle = paddle
        dynamicAnimator?.referenceView?.addSubview(self.paddle!)
    }
    
    func addBrick(brick: BrickView) {
        dynamicAnimator?.referenceView?.addSubview(brick)
        collider.addItem(brick)
        brickBehavior.addItem(brick)
    }
    
    func addBottomLoseBar(bottomLoseBar: UIView) {
        //dynamicAnimator?.referenceView?.addSubview(bottomLoseBar)
        //collider.addItem(bottomLoseBar)
        //brickBehavior.addItem(bottomLoseBar)
    }
    
    func removeBrick(brick: BrickView) {
        collider.removeItem(brick)
        brickBehavior.removeItem(brick)
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            //brick.frame.size.height = CGFloat(0.0)
            brick.frame = CGRectMake(brick.frame.origin.x, brick.superview!.frame.height, brick.bounds.size.width, 0)
        }) { (finished) -> Void in
            if finished {
                self.delegate?.brickDestroyed(brick)
                brick.removeFromSuperview()
            }
        }
    }
    
    func deleteBrick(brick: BrickView) {
        collider.removeItem(brick)
        brickBehavior.removeItem(brick)
        brick.removeFromSuperview()
    }
    
    func addBall(ball: BallView) {
        self.ball = ball
        dynamicAnimator?.referenceView?.addSubview(self.ball!)
        push.addItem(self.ball!)
        gravity.addItem(self.ball!)
        collider.addItem(self.ball!)
        ballBehavior.addItem(self.ball!)
    }
    
    func removeBall() {
        if self.ball != nil {
            push.removeItem(self.ball!)
            gravity.removeItem(self.ball!)
            collider.removeItem(self.ball!)
            ballBehavior.removeItem(self.ball!)
            self.ball!.removeFromSuperview()
            self.ball = nil
        }
    }
    
    func removeBall(ball: BallView) {
        push.removeItem(ball)
        gravity.removeItem(ball)
        collider.removeItem(ball)
        ballBehavior.removeItem(ball)
        ball.removeFromSuperview()
    }
}