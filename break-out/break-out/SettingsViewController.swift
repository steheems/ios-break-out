//
//  SettingsViewController.swift
//  break-out
//
//  Created by Gerrit Ruiter on 18/05/15.
//  Copyright (c) 2015 Gerrit Ruiter. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController {
    
    @IBOutlet weak var gravityUsesSensorSwitch: UISwitch!
    @IBOutlet weak var gravityStrengthSlider: UISlider!
    @IBOutlet weak var ballBouncinessSlider: UISlider!
    @IBOutlet weak var numberOfBallsStepper: UIStepper!
    @IBOutlet weak var numberOfBricksStepper: UIStepper!
    @IBOutlet weak var controlTypeSegmentedControl: UISegmentedControl!
    
    override func viewDidLoad() {
        gravityUsesSensorSwitch.on = SettingsManager().gravityUsesSensor
        gravityStrengthSlider.value = SettingsManager().gravityStrength
        ballBouncinessSlider.value = SettingsManager().ballBounciness
        numberOfBallsStepper.value = Double(SettingsManager().numberOfBalls)
        numberOfBricksStepper.value = Double(SettingsManager().numberOfBricks)
        controlTypeSegmentedControl.selectedSegmentIndex = find(Constants.controlTypeSegmentedControlValues, SettingsManager().controlMethod)!
        numberOfBallsStepperLabel.text = SettingsManager().numberOfBalls.description
        numberOfBricksStepperLabel.text = SettingsManager().numberOfBricks.description
        numberOfBricksPerRowStepperLabel.text = SettingsManager().numberOfBricksPerRow.description

    }
    
    private struct Constants {
        static let controlTypeSegmentedControlValues = ["Touch", "Accelerometer"]
    }
    
    @IBAction func gravityUsesSensorSwitch(sender: UISwitch) {
        SettingsManager().gravityUsesSensor = sender.on
    }
    
    @IBAction func gravityStrengthSlider(sender: UISlider) {
        SettingsManager().gravityStrength = sender.value
    }
    
    @IBAction func ballBouncinessSlider(sender: UISlider) {
        SettingsManager().ballBounciness = sender.value
    }

    @IBAction func numberOfBallsStepper(sender: UIStepper) {
        SettingsManager().numberOfBalls = Int(sender.value)
        numberOfBallsStepperLabel.text = Int(sender.value).description
    }
    
    @IBAction func numberOfBricksStepper(sender: UIStepper) {
        SettingsManager().numberOfBricks = Int(sender.value)
        numberOfBricksStepperLabel.text = Int(sender.value).description
    }
    
    @IBAction func numberOfBricksPerRowStepper(sender: UIStepper) {
        SettingsManager().numberOfBricksPerRow = Int(sender.value)
        numberOfBricksPerRowStepperLabel.text = Int(sender.value).description
    }
    
    @IBAction func controlTypeSegmentedControl(sender: UISegmentedControl) {
        SettingsManager().controlMethod = Constants.controlTypeSegmentedControlValues[sender.selectedSegmentIndex]
    }
    
    @IBOutlet weak var numberOfBallsStepperLabel: UILabel!
    @IBOutlet weak var numberOfBricksStepperLabel: UILabel!
    @IBOutlet weak var numberOfBricksPerRowStepperLabel: UILabel!
}
