//
//  BezierPathsView.swift
//  Dropit
//
//  Created by Erwin Heemsbergen on 19-04-15.
//  Copyright (c) 2015 Erwin Heemsbergen. All rights reserved.
//

import UIKit

class BezierPathsView: UIView {
    
    var bezierPaths = [String:UIBezierPath]()
    
    func setPath(path: UIBezierPath?, named name: String) {
        bezierPaths[name] = path
        setNeedsDisplay()
    }
    
    override func drawRect(rect: CGRect) {
        for (_, path) in bezierPaths {
            UIColor.redColor().setFill()
            path.fill()
        }
    }
}
