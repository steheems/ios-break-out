//
//  SettingsManager.swift
//  break-out
//
//  Created by Gerrit Ruiter on 18/05/15.
//  Copyright (c) 2015 Gerrit Ruiter. All rights reserved.
//

import Foundation

class SettingsManager {
    
    private let defaults = NSUserDefaults.standardUserDefaults()
    
    private struct Constants {
        static let gravityUsesSensor = "GameSettings.GravityUsesSensor"
        static let gravityStrength = "GameSettings.GravityStrength"
        static let ballBounciness = "GameSettings.BallBouncyness"
        static let numberOfBalls = "Gamesetting.Balls"
        static let numberOfBricks = "GameSettings.Bricks"
        static let numberOfBricksPerRow = "GameSettings.BricksPerRow"
        static let controlMethod = "GameSettings.ControlMethod"
        
        static let standardGravityUsesSensor = false
        static let standardGravityStrenght: Float = 1.0
        static let standardBallBounciness: Float = 1.0
        static let standardNumberOfBalls = 3
        static let standardNumberOfBricks = 20
        static let standardNumberOfBricksPerRow = 10
    }
    
    enum StandardControlMethod : String {
        case Touch = "Touch"
        case Accelerometer = "Accelerometer"
    }
    
    var gravityUsesSensor: Bool {
        get {
            return defaults.boolForKey(Constants.gravityUsesSensor)  ?? Constants.standardGravityUsesSensor
        }
        set {
            defaults.setBool(newValue, forKey: Constants.gravityUsesSensor)
        }
    }
    
    var gravityStrength: Float {
        get {
            return defaults.floatForKey(Constants.gravityStrength) ?? Constants.standardGravityStrenght
        }
        set {
            defaults.setFloat(newValue, forKey: Constants.gravityStrength)
        }
    }
    
    var ballBounciness: Float {
        get {
            return defaults.floatForKey(Constants.ballBounciness) ?? Constants.standardBallBounciness
        }
        set {
            defaults.setFloat(newValue, forKey: Constants.ballBounciness)
        }
    }
    
    var numberOfBalls: Int {
        get {
            if defaults.integerForKey(Constants.numberOfBalls) < 1 {
                return Constants.standardNumberOfBalls
            } else {
                return defaults.integerForKey(Constants.numberOfBalls)
            }
        }
        set {
            defaults.setInteger(newValue, forKey: Constants.numberOfBalls)
        }
    }
    
    var numberOfBricks: Int {
        get {
            if defaults.integerForKey(Constants.numberOfBricks) < 1 {
                return Constants.standardNumberOfBricks
            } else {
                return defaults.integerForKey(Constants.numberOfBricks)
            }
        }
        set {
            defaults.setInteger(newValue, forKey: Constants.numberOfBricks)
        }
    }
    
    var numberOfBricksPerRow: Int {
        get {
            if defaults.integerForKey(Constants.numberOfBricksPerRow) < 1 {
                return Constants.standardNumberOfBricksPerRow
            } else {
                return defaults.integerForKey(Constants.numberOfBricksPerRow)
            }
        }
        set {
            defaults.setInteger(newValue, forKey: Constants.numberOfBricksPerRow)
        }
    }
    
    var controlMethod: String {
        get {
            return defaults.stringForKey(Constants.controlMethod) ?? StandardControlMethod.Touch.rawValue
        }
        set {
            defaults.setObject(newValue, forKey: Constants.controlMethod)
        }
    }

}