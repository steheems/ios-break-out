//
//  GamePlayViewController.swift
//  break-out
//
//  Created by Gerrit Ruiter on 18/05/15.
//  Copyright (c) 2015 Gerrit Ruiter. All rights reserved.
//

import UIKit
import CoreMotion

class GamePlayViewController: UIViewController, UIDynamicAnimatorDelegate, BreakOutBehaviorDelegate {
    struct PathNames {
        static let Paddle = "Paddle"
        static let BottomLoseBar = "BottomLoseBar"
    }
    
    struct orientations {
        static let Portrait = "Portrait"
        static let Landscape = "Landscape"
    }
    
    var brickRowLimit: Int?
    
    var ballCount: Int?
    var ballCountStart: Int?
    var ballCountView: UITextView?
    
    var score: Int64 = 0
    
    var currentOrientation : String?
    
    var doRepositionPaddle = true
    
    var bricks = Set<BrickView>()
    
    var winAlert = UIAlertController(title: "Winner!", message: "You have won. Congratulations.", preferredStyle: UIAlertControllerStyle.Alert)
    var loseAlert = UIAlertController(title: "Lose", message: "You have lost. Try again?", preferredStyle: UIAlertControllerStyle.Alert)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentOrientation = calculateOrientation()
        startGame()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        unpause()
    }
    
    override func viewWillLayoutSubviews() {
        if !doRepositionPaddle && currentOrientation != calculateOrientation() {
            doRepositionPaddle = true
            currentOrientation = calculateOrientation()
            
            removeAllBricks()
            spawnBricks(SettingsManager().numberOfBricks)
        }
        
        repositionPaddle(doRepositionPaddle)
        doRepositionPaddle = false
        repositionBottomLoseBar()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        animator.removeAllBehaviors()
    }
    
    func calculateOrientation() -> String {
        if gameView.bounds.width > gameView.bounds.height {
            return orientations.Landscape
        } else {
            return orientations.Portrait
        }
    }
    
    func startGame() {
        winAlert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: {(UIAlertAction) -> Void in
            self.restartGame()
        }))
        loseAlert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: {(UIAlertAction) -> Void in
            self.restartGame()
        }))
        loseAlert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: nil))
        
        breakOutBehavior = BreakOutBehavior()
        breakOutBehavior!.delegate = self
        animator.addBehavior(breakOutBehavior)
        initPaddle()
        initBottomLoseBar()
        
        ballCount = SettingsManager().numberOfBalls
        ballCountStart = SettingsManager().numberOfBalls
        brickCount = SettingsManager().numberOfBricks
        brickRowLimit = SettingsManager().numberOfBricksPerRow
        spawnBricks(SettingsManager().numberOfBricks)
        drop()
    }
    
    func restartGame() {
        self.ballCount = self.ballCountStart
        self.drawLivesInNavigationBar()
        self.drawScore()
        self.removeAllBricks()
        self.spawnBricks(self.brickCount!)
        self.repositionPaddle(true)
        self.drop()
    }
    
    @IBOutlet var gameView: BezierPathsView!
    
    let manager = CMMotionManager()
    
    lazy var animator: UIDynamicAnimator = {
        let lazilyCreatedDynamicAnimator = UIDynamicAnimator(referenceView: self.gameView)
        lazilyCreatedDynamicAnimator.delegate = self
        return lazilyCreatedDynamicAnimator
        }()
    
    var breakOutBehavior: BreakOutBehavior?
    
    var sizeRatio = 20
    
    var brickCount: Int?
    
    var paddleSize: CGSize {
        let size = gameView.bounds.size.width / (CGFloat(sizeRatio) / 6)
        return CGSize(width: size, height: size / 8)
    }
    
    var bottomLoseBarSize: CGSize {
        return CGSize(width: gameView.frame.size.width, height: 5)
    }
    
    var dropSize: CGSize {
        let size = gameView.bounds.size.width / CGFloat(sizeRatio)
        return CGSize(width: size, height: size)
    }
    
    func drawScore() {
        navigationController?.navigationBar.topItem?.title = "Score: \(score)"
        //self.title = "Score: \(score)"
    }
    
    func drawLivesInNavigationBar() {
        if ballCountView == nil {
            ballCountView = UITextView()
        } else {
            ballCountView!.removeFromSuperview()
            ballCountView = UITextView()
        }
        
        var lives = "Balls: "
        
        for var i = ballCount!; i > 1; i-- {
            lives += "😀"
        }
        
        var lostBalls = SettingsManager().numberOfBalls - ballCount!
        for var i = lostBalls; i > 0; i-- {
            lives += "😡"
        }
        
        ballCountView!.text = lives
        ballCountView!.font = UIFont(name: ballCountView!.font.fontName, size: 18)
        ballCountView!.backgroundColor = nil
        ballCountView!.sizeToFit()
        navigationController!.navigationBar.addSubview(ballCountView!)
        navigationController!.navigationBar.bringSubviewToFront(ballCountView!)
        navigationController!.navigationBar.setNeedsDisplay()
    }
    
    func unpause() {
        animator.addBehavior(breakOutBehavior)
        
        var somethingChanged = false

        repositionPaddle(true)
        repositionBottomLoseBar()
        
        if brickCount != SettingsManager().numberOfBricks || brickRowLimit != SettingsManager().numberOfBricksPerRow || ballCountStart != SettingsManager().numberOfBalls  {
            brickCount = SettingsManager().numberOfBricks
            brickRowLimit = SettingsManager().numberOfBricksPerRow
            ballCountStart = SettingsManager().numberOfBalls
            ballCount = SettingsManager().numberOfBalls
            
            restartGame()
        }
        
        if breakOutBehavior?.useGravity != SettingsManager().gravityUsesSensor {
            breakOutBehavior?.useGravity = SettingsManager().gravityUsesSensor
            somethingChanged = true
        }
        
        if breakOutBehavior?.gravityStrength != SettingsManager().gravityStrength {
            breakOutBehavior?.gravityStrength = SettingsManager().gravityStrength
            somethingChanged = true
        }
        
        if breakOutBehavior?.bounciness != SettingsManager().ballBounciness {
            breakOutBehavior?.bounciness = SettingsManager().ballBounciness
            somethingChanged = true
        }
        
        if SettingsManager().controlMethod == SettingsManager.StandardControlMethod.Accelerometer.rawValue {
            if manager.deviceMotionAvailable {
                manager.deviceMotionUpdateInterval = 0.01
                manager.startDeviceMotionUpdatesToQueue(NSOperationQueue.mainQueue()) {
                    [weak self] (data: CMDeviceMotion!, error: NSError!) in
                    
                    var newX = self!.paddle!.frame.origin.x + CGFloat(data.gravity.x * 10)
                    var newY = self!.paddle!.frame.origin.y + CGFloat(data.gravity.z * 10 + 5)
                    
                    var lowerLimit = self!.gameView.bounds.size.height - (self!.paddleSize.height)
                    var upperLimit = self!.gameView.bounds.size.height - (self!.paddleSize.height * 4)
                    
                    if newX + (self!.paddleSize.width) < self!.gameView.bounds.width && newX > 0 {
                        self!.paddle!.frame.origin.x = newX
                        
                        if newY < lowerLimit && newY > upperLimit {
                            self!.paddle!.frame.origin.y = newY
                        }
                        
                        if self!.paddle != nil {
                            let path = UIBezierPath(ovalInRect: CGRect(origin: self!.paddle!.frame.origin, size: self!.paddleSize))
                            self!.breakOutBehavior!.addBarrier(path, name: PathNames.Paddle)
                        }
                    }
                }
            }
            somethingChanged = true
        } else if SettingsManager().controlMethod == SettingsManager.StandardControlMethod.Touch.rawValue {
            manager.stopDeviceMotionUpdates()
            somethingChanged = true
        }

        drawLivesInNavigationBar()
        drawScore()

        if somethingChanged {
            drop()
        }
    }
    
    var paddle: UIView?
    
    func initPaddle() {
        var paddleFrame = CGRect(origin: CGPointZero, size: paddleSize)
        paddleFrame.origin.x = gameView.bounds.size.width / 2 - paddleSize.width / 2
        paddleFrame.origin.y = gameView.bounds.size.height - tabBarController!.tabBar.bounds.size.height - navigationController!.navigationBar.bounds.size.height - paddleSize.height * 3
        
        let paddle = UIView(frame: paddleFrame)
        paddle.backgroundColor = UIColor.grayColor()
        
        breakOutBehavior!.addPaddle(paddle)
        
        let path = UIBezierPath(ovalInRect: CGRect(origin: paddleFrame.origin, size: paddleSize))
        breakOutBehavior!.addBarrier(path, name: PathNames.Paddle)
        //gameView.setPath(path, named: "Paddle")
        
        self.paddle = paddle
    }
    
    func repositionPaddle(force: Bool) {
        if force {
            paddle!.frame.origin.x = gameView.bounds.size.width / 2 - paddleSize.width / 2
            paddle!.frame.origin.y = gameView.bounds.size.height - paddleSize.height * 3
            let path = UIBezierPath(ovalInRect: CGRect(origin: paddle!.frame.origin, size: paddleSize))
            breakOutBehavior!.addBarrier(path, name: PathNames.Paddle)
        }
    }
    
    var bottomLoseBar: BottomLoseBarView?
    
    func initBottomLoseBar(){
        var bottomLoseBarFrame = CGRect(origin: CGPointZero, size: bottomLoseBarSize)
        bottomLoseBarFrame.origin.x = 0
        bottomLoseBarFrame.origin.y = gameView.bounds.size.height - tabBarController!.tabBar.bounds.size.height - navigationController!.navigationBar.bounds.size.height - bottomLoseBarSize.height
    
        bottomLoseBar = BottomLoseBarView(frame: bottomLoseBarFrame)
        //bottomLoseBar.backgroundColor = UIColor.redColor()
        
        //breakOutBehavior.addBottomLoseBar(bottomLoseBar)
        
        let path = UIBezierPath(rect: CGRect(origin: bottomLoseBarFrame.origin, size: bottomLoseBarSize))
        self.gameView.setPath(path, named: PathNames.BottomLoseBar)
        breakOutBehavior!.addBarrier(path, name: PathNames.BottomLoseBar)
    }
    
    func repositionBottomLoseBar() {
        var newY = gameView.bounds.size.height - bottomLoseBarSize.height
        bottomLoseBar!.frame.origin.y = newY
        let path = UIBezierPath(rect: CGRect(origin: bottomLoseBar!.frame.origin, size: bottomLoseBarSize))
        self.gameView.setPath(path, named: PathNames.BottomLoseBar)
        breakOutBehavior!.addBarrier(path, name: PathNames.BottomLoseBar)
    }
    
    func removeAllBricks() {
        for brick in bricks {
            breakOutBehavior?.deleteBrick(brick)
        }
        
        bricks.removeAll(keepCapacity: false)
    }
    
    func spawnBricks(brickCount: Int) {
        var rest = brickCount % brickRowLimit!
        
        var fullRows = Int(floor(Double(brickCount / brickRowLimit!)))
        
        var brickSize: CGSize {
            let size = gameView.bounds.size.width / CGFloat(brickRowLimit!)
            return CGSize(width: size, height: size)
        }
        
        for var i = 0; i < fullRows; i++ {
            for var j = 0; j < brickRowLimit!; j++ {
                var brickFrame = CGRect(origin: CGPoint(x: CGFloat(j) * brickSize.width, y: CGFloat(i) * brickSize.height), size: brickSize)
                var brick = BrickView(frame: brickFrame)
                brick.backgroundColor = UIColor.random
                breakOutBehavior!.addBrick(brick)
                bricks.insert(brick)
            }
        }
        
        for var i = 0; i < rest; i++ {
            var restRowDistance = gameView.bounds.size.width / CGFloat(rest)
            var brickFrame = CGRect(origin: CGPoint(x: CGFloat(i) * restRowDistance, y: CGFloat(fullRows) * brickSize.height), size: brickSize)
            var brick = BrickView(frame: brickFrame)
            brick.backgroundColor = UIColor.random
            breakOutBehavior!.addBrick(brick)
            bricks.insert(brick)
        }
    }
    
    @IBAction func pan(sender: UIPanGestureRecognizer) {
        var newX = self.paddle!.frame.origin.x + sender.translationInView(gameView).x
        var newY = self.paddle!.frame.origin.y + sender.translationInView(gameView).y
        
        var lowerLimit = gameView.bounds.size.height - (paddleSize.height)
        var upperLimit = gameView.bounds.size.height - (paddleSize.height * 4)
        
        if newX + (paddleSize.width) < gameView.bounds.width && newX > 0 {
            self.paddle!.frame.origin.x = newX
            
            if newY < lowerLimit && newY > upperLimit {
                self.paddle!.frame.origin.y = newY
            }
            
            if self.paddle != nil {
                let path = UIBezierPath(ovalInRect: CGRect(origin: self.paddle!.frame.origin, size: paddleSize))
                breakOutBehavior!.addBarrier(path, name: PathNames.Paddle)
            }
        }
        
        sender.setTranslation(CGPointZero, inView: gameView)
        
    }
    
    var lastDroppedView: UIView?

    @IBAction func tap(sender: UITapGestureRecognizer) {
        //drop()
        breakOutBehavior!.addPush(sender.locationInView(gameView).x, y: sender.locationInView(gameView).y)
        //breakOutBehavior.removePush()
    }
    
    func drop() {
        if breakOutBehavior!.ball == nil {
            var frame = CGRect(origin: CGPointZero, size: dropSize)
            //frame.origin.x = CGFloat.random(dropsPerRow) * dropSize.width
            frame.origin.x = gameView.bounds.size.width / 2 - dropSize.width / 2
            frame.origin.y = gameView.bounds.size.height / 2
            
            let dropView = BallView(frame: frame)
            dropView.backgroundColor = UIColor.blackColor()
            dropView.layer.cornerRadius = dropSize.width / 2
            dropView.layer.masksToBounds = true
            
            breakOutBehavior!.addBall(dropView)
            
            lastDroppedView = dropView
        }
    }
    
    
    func brickDestroyed(brick: BrickView) {
        bricks.remove(brick)
        
        switch(brick.backgroundColor!) {
        case UIColor.redColor():
            score += 100
        case UIColor.orangeColor():
            score += 200
        case UIColor.yellowColor():
            score += 300
        case UIColor.greenColor():
            score += 400
        case UIColor.blueColor():
            score += 500
        default:
            break
        }
        
        drawScore()
        
        if bricks.count < 1 {
            breakOutBehavior?.removeBall()
            winAlert.message = "You have won. Your score is: \(score)"
            presentViewController(winAlert, animated: true, completion: nil)
        }
    }
    
    func loseLife() {
        if ballCount < 2 {
            presentViewController(loseAlert, animated: true, completion: nil)
        } else if ballCount != nil {
            ballCount!--
            drawLivesInNavigationBar()
            repositionPaddle(true)
            drop()
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
    }

}

private extension CGFloat {
    static func random(max: Int) -> CGFloat {
        return CGFloat(arc4random() % UInt32(max))
    }
}

private extension UIColor {
    class var random: UIColor {
        switch arc4random() % 5 {
        case 0: return UIColor.redColor()
        case 1: return UIColor.orangeColor()
        case 2: return UIColor.yellowColor()
        case 3: return UIColor.greenColor()
        case 4: return UIColor.blueColor()
        default: return UIColor.purpleColor()
        }
    }
}